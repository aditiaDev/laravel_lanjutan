

// soal nomor 1. Mengubah arrow function
 const golden = goldenFunction => { 
     console.log('Soal No.1 ')
    console.log("this is golden!!")
}

golden()

// No.2 object literal
const newFunction = function literal(firstName, lastName){
    return {
      firstName, lastName,
      fullName: function(){
          let data = `${firstName } ${lastName}`
        console.log('Soal No.2')
        console.log(data)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

//   no. 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
// es5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// es6
let{firstName, lastName, destination, occupation, spell} = newObject
// Driver code
console.log('Soal No.3')
console.log(firstName, lastName, destination, occupation, spell)


// no 4.

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east];
//Driver Code
console.log('soal no 4')
console.log(combined)


// No.5 
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, 
${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
// Driver Code 
console.log('Soal No.5 ')
console.log(before)

<?php

namespace App\Models\Fakultas;

use App\Models\Jurusan;
use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'fakultas',
    ];

    public function getRouteKeyName(){
        return 'fakultas';
    }

    public function jurusans(){
        return $this->hasMany(Jurusan::class);
    }
}

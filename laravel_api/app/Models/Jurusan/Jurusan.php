<?php

namespace App\Models\Jurusan;

use App\Models\Fakultas\Fakultas;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $fillable = [
        'jurusan',
    ];

    public function fakultas(){
        return $this->belongsTo(Fakultas::class);
    }
}

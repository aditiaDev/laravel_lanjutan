<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->string('kode_buku');
            $table->string('judul');
            $table->string('pengarang',50);
            $table->string('tahun_terbit',5);
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            
            $table->primary('kode_buku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}

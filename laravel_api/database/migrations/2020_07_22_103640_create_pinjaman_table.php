<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjaman', function (Blueprint $table) {
            $table->string('kode_pinjam');
            $table->datetime('tanggal_pinjam');
            $table->datetime('tanggal_batas_pinjam');
            $table->datetime('tanggal_kembali')->nullable();
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->string('nim');
            $table->boolean('status_ontime')->nullable();

            $table->foreign('nim')->references('nim')->on('mahasiswa')->onDelete('cascade');
            $table->primary('kode_pinjam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjaman');
    }
}

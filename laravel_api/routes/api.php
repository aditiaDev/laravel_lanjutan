<?php

Route::namespace('Auth')->group(function(){
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::namespace('Fakultas')->middleware('auth:api')->group(function(){
    Route::post('create_fakultas', 'FakultasController@store');
});

Route::get('fakultas/{id}','Fakultas\FakultasController@show');
Route::get('fakultas','Fakultas\FakultasController@index');

Route::get('user', 'UserController');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SayaController extends Controller
{
    public function super_admin()
    {
        return 'ini halaman superadmin';
    }
    
    public function admin()
    {
        return 'ini halaman admin';
    }
    
    public function guest()
    {
        return 'ini halaman guest';
    }
}

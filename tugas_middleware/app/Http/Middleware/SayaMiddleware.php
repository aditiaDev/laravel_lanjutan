<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class SayaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->route()->uri());
        // cek uri
        if($request->route()->uri()=='route-1')
        {
            // cek role_id
            if(Auth::user()->isRole() =='super_admin'){
                return $next($request);
            }else{
                abort(403);
            }
        }
        elseif($request->route()->uri()=='route-2')
        {
            if(Auth::user()->isRole() == 'admin' || Auth::user()->isRole() == 'super_admin')
            {
                return $next($request);
            }else{
                abort(403);
            }
        }
        else{
            return $next($request);
        }
    }
}

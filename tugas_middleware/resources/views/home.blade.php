@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }} sebagai <strong>{{Auth::user()->isRole()}}</strong>
                </div>
                <div class="card-body">
                    <a href="{{ url('/route-3') }}" class="btn btn-success">Route Guest</a>
                    <a href="{{ url('/route-2') }}" class="btn btn-success">Route Admin</a>
                    <a href="{{ url('/route-1') }}" class="btn btn-success">Route Superadmin</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['SayaMiddleware', 'auth'])->group(function(){
    Route::get('/route-1', 'SayaController@super_admin')->name('super_admin');
    Route::get('/route-2', 'SayaController@admin')->name('admin');
    Route::get('/route-3', 'SayaController@guest')->name('guest');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
